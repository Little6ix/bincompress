package bincompress;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	LzwCompressTest.class,
	LzwDecompressTest.class,
	LzwCompressDecompressTest.class,
	HuffmanCompressTest.class,
	HuffmanDecompressTest.class,
	HuffmanCompressDecompressTest.class,

})

public class CompressDecompressSuite {

}
