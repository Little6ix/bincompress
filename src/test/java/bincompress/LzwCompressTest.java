package bincompress;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class LzwCompressTest {

	private static final ClassLoader loader = LzwCompressTest.class.getClassLoader();
	private String filePath;
	private String outputPath;
	
	@Parameterized.Parameters
	public static List paths() {
		return Arrays.asList(new Object[][]{
				{"totoland.txt","totoland.txt.lzw"},
				{"fileWithSpecialChars.txt","fileWithSpecialChars.txt.lzw"},
				{"alice29.txt","alice29.txt.lzw"},
				{"emptyFile.txt","emptyFile.txt.lzw"}
		});
	}
	
	public LzwCompressTest(String filepath, String output) {
		this.filePath=filepath;
		this.outputPath=output;
		
		//backup to avoid overwriting
		String fullpath = loader.getResource(filePath).getPath();
		String fullPathBkp = fullpath+".bkp";
		try {
			FileUtils.copyFile(new File(fullpath), new File(fullPathBkp));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@Test
	public void compressFileTest() {
		LzwCompress compressAlgo = new LzwCompress();
		String path=null;
		try {
			Field pathField=LzwCompress.class.getDeclaredField("path");
			pathField.setAccessible(true);
			path = loader.getResource(filePath).getPath(); 
			pathField.set(compressAlgo, path);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		compressAlgo.run();
		String pathCompress = path.replace(filePath, outputPath);
		File f = new File(pathCompress);
		assertTrue(f.exists());
	}

}
