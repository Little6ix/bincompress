package bincompress;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class HuffmanCompressDecompressTest {

	private static final ClassLoader loader = HuffmanCompressDecompressTest.class.getClassLoader();
	private String filePathO;
	private String filePathD;
	
	@Parameterized.Parameters
	public static List paths() {
		return Arrays.asList(new Object[][]{
			{"totoland.txt.bkp","totoland.txt"},
			{"fileWithSpecialChars.txt.bkp","fileWithSpecialChars.txt"},
			{"alice29.txt.bkp","alice29.txt"},
		});
	}
	
	public HuffmanCompressDecompressTest(String pathOrigine, String pathDecomp) {
		this.filePathO=pathOrigine;
		this.filePathD=pathDecomp;
	}
	
	@Test
	public void decompressGivesSameContentAsOriginalFileTest() {
		String pathOriginal = loader.getResource(filePathO).getPath(); 
		String pathDecompressed = loader.getResource(filePathD).getPath();
		try {
			assert(FileUtils.contentEqualsIgnoreEOL(new File(pathOriginal), new File(pathOriginal),null));
		} catch (IOException e) {
			fail("Le fichier d'origine et celui regénéré ne sont pas identiques");
		}
	}
	


}
