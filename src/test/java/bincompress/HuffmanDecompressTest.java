package bincompress;

import static org.junit.Assert.*;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class HuffmanDecompressTest {
	
	private static final ClassLoader loader = HuffmanDecompressTest.class.getClassLoader();
	private String filePath;
	private String outputPath;
	
	@Parameterized.Parameters
	public static List paths() {
		return Arrays.asList(new Object[][]{
				{"totoland.txt.huff","totoland.txt"},
				{"fileWithSpecialChars.txt.huff","fileWithSpecialChars.txt"},
				{"alice29.txt.huff","alice29.txt"},
		});
	}
	
	public HuffmanDecompressTest(String path, String output) {
		this.filePath=path;
		this.outputPath=output;
	}
	
	@Test
	public void decompressFileTest() {
		HuffmanDecompress decompressAlgo = new HuffmanDecompress();
		String path = null;
		try {
			Field pathField=HuffmanDecompress.class.getDeclaredField("path");
			pathField.setAccessible(true);
			path = loader.getResource(filePath).getPath(); 
			pathField.set(decompressAlgo, path);
		} catch (Exception e) {
			fail();
		}
		decompressAlgo.run();
		String pathDecompress = path.replace(filePath, outputPath);
		File f = new File(pathDecompress);
		assertTrue(f.exists());
	}

}
