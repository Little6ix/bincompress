package bincompress;

import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class HuffmanCompressTest {
	
	private static final ClassLoader loader = HuffmanCompressTest.class.getClassLoader();
	private String filePath;
	private String outputPath;
	
	@Parameterized.Parameters
	public static List paths() {
		return Arrays.asList(new Object[][]{
				{"totoland.txt","totoland.txt.huff"},
				{"fileWithSpecialChars.txt","fileWithSpecialChars.txt.huff"},
				{"alice29.txt","alice29.txt.huff"},
		});
	}
	
	public HuffmanCompressTest(String path, String output) {
		this.filePath=path;
		this.outputPath=output;
	}
	
	@Test
	public void compressFileTest() {
		HuffmanCompress compressAlgo = new HuffmanCompress();
		String path=null;
		try {
			Field pathField=HuffmanCompress.class.getDeclaredField("path");
			pathField.setAccessible(true);
			path = loader.getResource(filePath).getPath(); 
			pathField.set(compressAlgo, path);
		} catch (Exception e) {
			e.printStackTrace();
			fail();
		}
		compressAlgo.run();
		String pathCompress = path.replace(filePath, outputPath);
		File f = new File(pathCompress);
		assertTrue(f.exists());
	}

}
