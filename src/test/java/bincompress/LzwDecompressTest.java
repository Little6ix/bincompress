package bincompress;

import static org.junit.Assert.*;

import java.io.File;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class LzwDecompressTest {

	private static final ClassLoader loader = LzwDecompressTest.class.getClassLoader();
	private String filePath;
	private String outputPath;
	
	@Parameterized.Parameters
	public static List paths() {
		return Arrays.asList(new Object[][]{
				{"totoland.txt.lzw","totoland.txt"},
				{"fileWithSpecialChars.txt.lzw","fileWithSpecialChars.txt"},
				{"alice29.txt.lzw","alice29.txt"},
				{"emptyFile.txt.lzw","emptyFile.txt"}
		});
	}
	
	public LzwDecompressTest(String path, String output) {
		this.filePath=path;
		this.outputPath=output;
	}
	
	@Test
	public void decompressFileTest() {
		LzwDecompress decompressAlgo = new LzwDecompress();
		String path = null;
		try {
			Field pathField=LzwDecompress.class.getDeclaredField("path");
			pathField.setAccessible(true);
			path = loader.getResource(filePath).getPath(); 
			pathField.set(decompressAlgo, path);
		} catch (Exception e) {
			fail();
		}
		decompressAlgo.run();
		String pathCompress = path.replace(filePath, outputPath);
		File f = new File(pathCompress);
		assertTrue(f.exists());
	}
}
