package bincompress;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;

@RunWith(Parameterized.class)
public class LzwCompressDecompressTest {

	private static final ClassLoader loader = LzwCompressDecompressTest.class.getClassLoader();
	private String filePathO;
	private String filePathD;
	
	@Parameterized.Parameters
	public static List paths() {
		return Arrays.asList(new Object[][]{
				{"totoland.txt.bkp","totoland.txt"},
				{"fileWithSpecialChars.txt.bkp","fileWithSpecialChars.txt"},
				{"alice29.txt.bkp","alice29.txt"},
				{"emptyFile.txt.bkp","emptyFile.txt"}
		});
	}
	
	public LzwCompressDecompressTest(String pathOrigine, String pathDecomp) {
		this.filePathO=pathOrigine;
		this.filePathD=pathDecomp;
	}
	
	@Test
	public void decompressGivesSameContentAsOriginalFileTest() {
		String pathOriginal = loader.getResource(filePathO).getPath(); 
		String pathDecompressed = loader.getResource(filePathD).getPath();
		/*try(BufferedReader brOrigin = new BufferedReader(new FileReader(pathOriginal));
			BufferedReader brDecomp = new BufferedReader(new FileReader(pathDecompressed)))
		{
			String stO = brOrigin.readLine();
			String stD = brDecomp.readLine();
			while(stO !=null && stD !=null) {
				if(!stO.equals(stD)) {
					fail();
				}else {
					stO = brOrigin.readLine();
					stD = brDecomp.readLine();
				}
			}
			assert(brOrigin.readLine() == null && brDecomp.readLine() == null);
		}catch(Exception e) {
			fail();
		}*/
		try {
			assert(FileUtils.contentEqualsIgnoreEOL(new File(pathOriginal), new File(pathOriginal),null));
		} catch (IOException e) {
			fail("Le fichier d'origine et celui regénéré ne sont pas identiques");
		}
	}

}
