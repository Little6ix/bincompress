package bincompress;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LzwDecompress implements Runnable{
	
	private String path;
	private final int INITIAL_DICTIONNARY_SIZE = 127;
	private Map<Integer, String> dictionnary;
	
	public LzwDecompress() {}
	
	public LzwDecompress(String path) {
		this.path=path;
	}

	public void run() {
		List<Integer> input = new ArrayList<>();
		InputStreamReader is=null;
		BufferedReader buf =null;
		try {
			is = new InputStreamReader(new FileInputStream(path));
			buf = new BufferedReader(is);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
			        
		Integer code=null;
		try {
			code = buf.read();
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		try {
			while(code != -1){
				input.add(code); 
				code = buf.read();
			}
		}catch(IOException e) {
			e.printStackTrace();
		}
		if(input.isEmpty()) {
			generateTextFile("");
		}else {
			decompress(input);
		}
	}
	

	public void decompress(List<Integer> input) {
		String output;
		String word="",c="";
		int unicode,old_unicode;
		int index = INITIAL_DICTIONNARY_SIZE;
		
		initDictionnary();
		
		old_unicode=input.remove(0);
		output=dictionnary.get(old_unicode);
		//tant que not Empty
		while(!input.isEmpty()) {
			//read 1st value of input
			unicode = input.remove(0);
			
			if(dictionnary.containsKey(unicode)) {
				word=dictionnary.get(unicode);
			}else {
				word=dictionnary.get(old_unicode);
				word+=c;
			}
			output+=word;
			c=word.substring(0,1);
			String ajout=dictionnary.get(old_unicode)+c;
			dictionnary.put(index++, ajout);
			old_unicode=unicode;
		}
		
		generateTextFile(output);
	}
	
	private void generateTextFile(String output) {
		//String outputPath = path.replaceAll("\\.[^.]*$", ".lzwd");
		String outputPath = path.replaceAll(".lzw$", "");
		File file = new File(outputPath);
		Writer writer=null;
		BufferedWriter buffer = null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(file),"UTF-8");
			buffer = new BufferedWriter(writer);
			buffer.write(output);
		} catch (IOException e) {
			e.printStackTrace();
		}finally {
			try {
				buffer.flush();
				buffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}

	private void initDictionnary() {
		dictionnary = new HashMap<Integer, String>();
		for(int i = 0; i<INITIAL_DICTIONNARY_SIZE;i++) {
			dictionnary.put(i,String.valueOf((char)i));
		}
	}
	

}
