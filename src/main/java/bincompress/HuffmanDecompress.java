package bincompress;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;

public class HuffmanDecompress implements Runnable{
	
	private String path;
	
	HuffmanCompress hf = new HuffmanCompress();
	
	public HuffmanDecompress() {}
	
	public HuffmanDecompress(String path) {
		this.path=path;
	}

	public void run() {
		try {
	        File file = new File(this.path);
	        hf.countFrequency(file);
			Huffman root = hf.encode();
			decompress(root,file);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void decompress(Huffman root, File file) throws IOException {
		
		String outputPath = path.replaceAll(".huff$", "");
        Huffman hRoot = root;
        ArrayList<Byte> contByte = new ArrayList<>();  
    	BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8"));
        
    	byte b;
        while((b = (byte)in.read())!= -1)
            {
                contByte.add(b);
            }   

        StringBuilder output = new StringBuilder();
        StringBuffer contents = new StringBuffer();
        for(byte b1:contByte)
        {
            byte cb=b1;
            contents.append(String.format("%8s", Integer.toBinaryString(b1 & 0xFF)).replace(' ', '0'));
        }
        System.out.println("\ncontents decoded="+contents);
        for(int i=0;i<contents.length();i++)
        {
            char ch = contents.charAt(i);
            if(ch=='0')
                root =  root.getlChild();
            else if(ch=='1')
                root = root.getrChild();
            if(root.getrChild()==null && root.getlChild()==null)
            {
                //System.out.print(root.getCharacter());
                output.append(root.getCharacter());
                root=hRoot;
            }
        }
        try(FileWriter fw = new FileWriter(outputPath);)
        {
            fw.write(output.toString());
        } catch (IOException ex) {
        }
	}

}
