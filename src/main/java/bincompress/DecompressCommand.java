package bincompress;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "decompress", mixinStandardHelpOptions = true)
public class DecompressCommand implements Runnable{

	@Option(names = {"-H", "--Help"})
	private boolean help;
	@Option(names = {"-f", "--file"}, required=true, description ="Specify the path to the file to decompress")
	private String path;
	
	private String LZW_EXTENSION=".lzw";
	private String HUFF_EXTENSION=".huff";
    
	public static void main(String[] args) {
    	 int exitCode = new CommandLine(new DecompressCommand()).execute(args);
         System.out.println(exitCode);
    }
	
	public void run() {
		if(path.endsWith(LZW_EXTENSION)) {
			new LzwDecompress(path).run();
		}else if(path.endsWith(LZW_EXTENSION)) {
			new HuffmanDecompress(path).run();
		}else {
			help=true;
		}
		if(help) {
			System.out.println("decompresses only files ending with "+LZW_EXTENSION+" or "+HUFF_EXTENSION);
		}
		
	}

}
