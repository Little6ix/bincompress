package bincompress;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "lzw",mixinStandardHelpOptions = true)
public class LzwCompress implements Runnable{
	
	@Option(names = {"-f", "--file"}, required=true)
	private String path;
	
	private final int INITIAL_DICTIONNARY_SIZE = 127;
	private Map<String, Integer> dictionnary;

	public void run() {
		File file = new File(this.path);
		InputStream is=null;
		try {
			is = new FileInputStream(file);
		} catch (FileNotFoundException e2) {
			e2.printStackTrace();
		}
		BufferedReader buf = new BufferedReader(new InputStreamReader(is));
		        
		String fileAsString="";
		char c;
		try {
			while((c =(char) buf.read())!= (char)-1) {
				fileAsString += Character.toString(c);
				
			}
		} catch (IOException e2) {
			e2.printStackTrace();
		}
		compress(fileAsString);
	}

	private void compress(String input) {
		if(null==input) return;
		/*
		 *      1. Initialize the dictionary to contain all strings of length one.
    			2.Find the longest string W in the dictionary that matches the current input.
    			3.Emit the dictionary index for W to output and remove W from the input.
    			4.Add W followed by the next symbol in the input to the dictionary.
    			5.Go to Step 2.
		 */
		
		List<Integer> output = new ArrayList<>();
		int dictionnary_size = INITIAL_DICTIONNARY_SIZE;
		
		String word = "";
		String caractere,p;
		
		initDictionnary();
		
		while (!input.isEmpty()) {
			//recuperation du 1er caractere
			caractere = input.substring(0, 1);
			input = input.substring(1);
			//creation du nv unicode avec lettre precendente
			p = word.concat(caractere);
			if(dictionnary.containsKey(p)) {
				word = p;
			}else {
				dictionnary.put(p, dictionnary_size++);
				output.add(dictionnary.get(word));
				word = caractere;
			}
			
		}
		//System.out.println(output);
		generateLzwFile(output);
	}

	private void generateLzwFile(List<Integer> output) {
		//String outputPath = path.replaceAll("\\.[^.]*$", ".lzw");
		String outputPath = path.concat(".lzw");
		File file = new File(outputPath);
		Writer writer=null;
		try {
			writer = new OutputStreamWriter(new FileOutputStream(file));
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		BufferedWriter buffer = new BufferedWriter(writer);
		try {
			for(Integer i : output) {
				buffer.write(i);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			buffer.flush();
			buffer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	private void initDictionnary() {
		dictionnary = new HashMap<String, Integer>();
		for(int i = 0; i<INITIAL_DICTIONNARY_SIZE;i++) {
			dictionnary.put(String.valueOf((char)i), i);
		}
	}
	

}
