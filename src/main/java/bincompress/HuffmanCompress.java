package bincompress;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.PriorityQueue;

import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "huffman",mixinStandardHelpOptions = true)
public class HuffmanCompress implements Runnable {
	
	@Option(names = {"-f","--file"}, required = true)
	private String path;
	
    private static HashMap<Character,String> encoder;
    public static int frequencyHolder[];
    public static int totalFrequency = 0;//No. of characters in original file
    public static int totalEncoded = 0;//No. of characters in encoded file
    public static int totalDecoded = 0;//No. of characters in decoded file
	
	public void run() {
		
        try {
    		File file = new File(this.path);
			this.countFrequency(file);
	        Huffman root = this.encode();
			this.generateCompressedFile(file);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
    /*
    Count the frequency of various ASCII characters
    */
    void countFrequency(File file) throws IOException
    {
        frequencyHolder = new int[256];        
    	BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8"));
        int c;           
        while((c = in.read())!= -1)
            if(c<256)
            {
                frequencyHolder[c]++;
                totalFrequency++;
            }            
    }
	
	/*
	    Sets up the min-Heap
	*/
	public Huffman encode()
	{
	    int n = frequencyHolder.length;
	    
	    PriorityQueue <Huffman> minheap= new PriorityQueue<>(n,FREQUENCY_COMPARATOR);
	    
	    char c;int a;
	    for(int i=0;i<n;i++)
	    {
	        if(frequencyHolder[i]!=0)
	            minheap.add(new Huffman((char)i,frequencyHolder[i]));
	    }
	    
	    Huffman z=null;
	    while(minheap.size()>1)
	    {
	        Huffman x=minheap.peek();
	        minheap.poll();
	        Huffman y=minheap.peek();
	        minheap.poll();
	        x.setCode("0");
	        y.setCode("1");
	        z=new Huffman();
	        //z=z.create(x,y);
	        z.setCharacter('\u09B8');
	        z.setFrequency(x.getFrequency()+y.getFrequency());
	        z.setlChild(x);
	        z.setrChild(y);
	        minheap.add(z);            
	    }
	    
	    encoder = new HashMap<>();
	    
	    Huffman root=z;
	    traverse(root,"");
	    
	    System.out.println("");
	    
	    for(int i=0;i<n;i++)
	       if(frequencyHolder[i]!=0)
	        System.out.println((char)i+"\t"+frequencyHolder[i]+"\t"+encoder.get((char)i));
	    return root;
	}
	/*
	 Comparator to check the char with lowest frequency
	*/        
	private static final Comparator<Huffman> FREQUENCY_COMPARATOR = (Huffman o1, Huffman o2) -> (int) (o1.getFrequency()-o2.getFrequency());
	/*
	recursively set up the codeword for each character
	*/
	private void traverse(Huffman root,String s)
	{   
	    if(root.getCode()!=null)
	        s+=root.getCode();
	    if(root.getlChild()==null && root.getrChild()==null && root.getCharacter()!='\u09B8')
	        {
	            //System.out.println(root.getCharacter()+":"+s);
	            encoder.put(root.getCharacter(), s);
	            return;
	        }
	    
	     traverse(root.getlChild(), s);
	     traverse(root.getrChild(), s);
	}
	
	private void generateCompressedFile(File file) throws IOException
	{
		String outputPath = path.concat(".huff");
	    ArrayList<Byte> contByte = new ArrayList<>();  
	    StringBuffer contents=new StringBuffer();
	    
	    BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(file),"UTF8"));
	    int c;
	    while((c = in.read())!= -1)
	        if(c<256)
	        {
	            
	            contents.append(encoder.get((char)c));  
	            System.out.print(encoder.get((char)c)+",");
	        }
	    try
	    {
	        byte b=0;
	        System.out.println("\nspill:"+contents);
	        int len;
	        for(len=contents.length();len<contents.length()+8;len++)
	            if(len%8==0)break;
	         
	        
	       for(int i=0;i<len;i++)
	        {
	            if(i>=contents.length())
	                b = (byte)(b<<1);
	            else
	            {
	                b = (byte)((b<<1)|Character.getNumericValue(contents.charAt(i)));
	            }
	            if(((i+1)%8==0&&i!=0)||i==len-1)
	                {
	                    contByte.add((byte)b);
	                    b=0;
	                }
	            
	        }
	    }
	    catch(NullPointerException ne){;}
	    try(FileOutputStream fos = new FileOutputStream(outputPath);)
	    {
	        for(byte b:contByte)
	        {
	            
	            fos.write(b);
	        }
	    } catch (IOException ex) {
	    }
	    
	}
	

}


