package bincompress;

import picocli.CommandLine;
import picocli.CommandLine.Command;
import picocli.CommandLine.Option;

@Command(name = "compress", mixinStandardHelpOptions = true,
		subcommands = {
			HuffmanCompress.class,
			LzwCompress.class
		}
	)
public class CompressCommand implements Runnable{
	
	@Option(names = {"-H", "--Help"})
	private boolean help;
    
	public static void main(String[] args) {
    	 int exitCode = new CommandLine(new CompressCommand()).execute(args);
         System.out.println(exitCode);
    }
	
	public void run() {
		if(help) {
			//TODO better message
			System.out.println("lzw or huffman");
		}
	}
	

}
