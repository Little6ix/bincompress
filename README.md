# REPORT VV - Bastien Ollivo - Boris Yao

Since the application must implement at least two algorithms, we have chosen the default ones, i.e. the LZW (Lempel-Ziv-Welch) and Huffman algorithms.
We used the Picocli tool for the creation of the command line application. It can be launched following the following template after build:

### Lzw
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar compress lzw -f ./path/to/file.ext  
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar decompress -f ./path/to/file.ext.lzw  

### Huffman
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar compress huffman -f ./path/to/file.ext  
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar decompress -f ./path/to/file.ext.huff

## Tests of the algorithms
We used the Junit framework for the implementation of our tests
We have implemented 3 classes by tests by algorithms, i.e. 6 classes that can be found in the /src/test/java directory.

They are made up as follows:

 - the compression: the class which deals with compression is composed of several identical tests which run the algorithm on several different files and check that the file is well generated.
 - the decompression: the class which takes care of the decompression is identical to the one in the previous point, but for the decompression part of the algorithm.
 - the comparison: this class compares the original files and their decompressed versions line by line. It checks that the files are indeed identical.


## Gitlab CI
For continuous integration we have a gitlab.yml file that contains the different phases of construction of our project.
gitlab.yml runs the testSuite but also the other tests individually => plugin maven-surefire

link : https://gitlab.com/Little6ix/bincompress/pipelines

## SonarQube 
The sonarqube page of our project gives an overview of the quality of the code and the coverage rate of the different tests.
Sonar analysis is done with gitlab CI from the gitlab.yml file.
We used sonarcloud which is a public service of sornarqube hosting.

sonarqube server address: https://sonarcloud.io/dashboard?id=bincompress





# RAPPORT VV - Bastien Ollivo - Boris Yao

L'application devant implémenter au moins deux algorithmes, nous avons choisi ceux proposés par défaut à c'est-à-dire les algorithmes LZW (Lempel-Ziv-Welch) et Huffman.
Nous avons utilisé l'outil Picocli pour la création de l'application en ligne de commande. Elle peut etre lancé en suivant le modèle suivant après build:

### Lzw
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar compress lzw -f ./path/to/file.ext  
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar decompress -f ./path/to/file.ext.lzw

### Huffman
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar compress huffman -f ./path/to/file.ext  
java -jar bincompress-0.0.1-SNAPSHOT-jar-with-dependencies.jar decompress -f ./path/to/file.ext.huff

## Tests des algorithmes
Nous avons utilisé le framework Junit pour l'implémentations de nos tests
Nous avons implémentés 3 classes par de tests par algorithmes soit 6 classes qui de trouve dans le repertoire /src/test/java

Ils sont constitués de la façon suivante:

 - la compression : la classe qui s'occupe de la compression est composée de plusieurs tests identiques qui exécutent l'algorithme sur plusieurs fichiers différents et vérifient que le fichier est bien généré.
 - la décompression : la classe qui s'occupe de la décompression est identique à les mêmes fonctionnalités que celle du point précédent, mais pour la partie decompression de l'algorithme.
 - la comparaison : cette classe compare les fichiers d'origine et leur versions décompressées ligne par ligne. Il vérifie que les fichiers sont bien identiques.


## Gitlab CI
Pour l'intégration continue nous avons un fichier gitlab.yml qui contient les differentes phases de construction de notre projet.
Des artifacts sont générés pour pouvoir analyser le code coverage et récupérer les fichiers de test.

Lien : https://gitlab.com/Little6ix/bincompress/pipelines

## SonarQube 
La page un sonarqube de notre projet donne un aperçu global de la qualité du code et du taux de couvertures des differents tests
L'analyse sonar est faite avec gitlab CI à partir du fichier gitlab.yml.
Nous avons utilisé sonarcloud qui est un service public d'hebergement sornarqube

adresse du serveur sonarqube : https://sonarcloud.io/dashboard?id=bincompress
